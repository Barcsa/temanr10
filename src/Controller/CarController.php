<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Entity\Car;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class CarController extends AbstractController
{
    /**
     * @Route("/createCar", name="createCar")
     */
    public function new(Request $request)
    {
        $car = new Car();
        $formForCar = $this->createFormBuilder($car)
        ->add('brand', TextType::class)
        ->add('year', NumberType::class)
        ->add('ADD', SubmitType::class, ['label' => 'ADD car in db'])
        ->getForm();
        $formForCar->handleRequest($request);
        if ($formForCar->isSubmitted()) {
            $car = $formForCar->getData();
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($car);
            $manager->flush();
        } 
        return $this->render('car.html.twig', [
            'form' => $formForCar->createView(),
        ]); 
    }
}
